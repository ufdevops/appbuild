job("Dev_UComm") {
	scm {
		git {
			remote {
				url('https://unifirst.visualstudio.com/UniFirst/_git/ucom')
				credentials('da1f23b8-10c2-40f7-8434-f23f8a66f0fc')
			}
			branch('origin/pr/${pullRequestId}/merge')
		}
	}
}
